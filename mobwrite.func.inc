<?php

/**
 * @file
 * Non-hook, MobWrite Module functions. Included by mobwrite.module.
 *
 * This file includes all of the functions used by the MobWrite Module except
 * those that are implemented as Drupal Hooks. This file is included on-demand
 * by mobwrite.module.
 */


/**
 * Custom submit handler to delete MobWrite "session-like" data from custom
 * table and from sync gateway.
 */
function mobwrite_session_clear($nid) {
    $nid = (!$nid) ? menu_get_object()->nid : $nid;

  // Remove user from participants
  mobwrite_participant_del($GLOBALS['user']->uid, $nid);

  // Check if any more participants, if not clear mobwrite session data
  if(!mobwrite_participant_is(NULL, $nid)) {
    // take care of mobwrite_variables
    mobwrite_variable_del('mode_node_' . $nid);
    mobwrite_variable_del('share_group_node_' . $nid);

    // take are of sync gateway data
    drupal_add_js('jQuery(document).ready(function () {
      mobwrite.shared = {};
      mobwrite.nullifyAll = true;
    });', 'inline');
  }
} // mobwrite_session_clear()


/**
 * @todo expand user message to be more helpful
 * Sets the share mode of a particular node. Called via callback.
 *
 * @param $node
 *  The Drupal node of the share mode to set.
 * @param $mode
 *  The share mode to set, current cases are 'no', 'url', and 'role'.
 * @param $key
 *  Used verify the callback URL by comparing param to the share group from db.
 *
 * @return
 *  Does not return, but either exits with user message or calls
 *  drupal_set_message() followed by a drupal_goto().
 */
function mobwrite_share_mode($node = NULL, $mode = NULL, $key = NULL) {

  // first, make sure the key is valid
  $nid = $node->nid;
  $share_group = mobwrite_variable_get('share_group_node_' . $nid, NULL);

  //echo "share_group: " . $share_group . " | key: " . $key . " | ";
  if ($key != $share_group) {
    // key is invalid, so set mode  to 'no access'
    mobwrite_variable_set('mode_node_' . $nid, 'no');
    //drupal_set_message(t('The key provided does not match the MobWrite Session:') . '<br>' . $key . ' vs ' . mobwrite_variable_get('share_group_node_' . $nid, NULL), 'status', FALSE);
    //drupal_goto('node/' . $nid, drupal_get_destination());
    //exit(t('The key provided does not match the MobWrite Session. Set to') . ' no access.');
    exit($key . " is not = to " . $share_group);
  }


  // if mode is going to change, reset the mobwrite session
  if($mode != mobwrite_variable_get('mode_node_' . $nid, NULL)) {
    // del all participants for this $nid
    mobwrite_participant_del(NULL, $nid);
    // @todo can't reset share_group until it the function also resets the
    // attached settings' shareGroup value
    // del old share_group and get new one.
    //mobwrite_share_group_reset();
  }

  // options for each passed $mode
  switch ($mode) {
    // case 'get' is uses to return an existing set mode.
    case 'get':
      exit(mobwrite_variable_get('mode_node_' . $nid, 'not set'));

    // share mode is 'no access'
    case 'no':
      //drupal_set_message(t('MobWrite access is set for only the initiating editor to have access.'), 'status', FALSE);
      mobwrite_variable_set('mode_node_' . $nid, 'no');
      // return to the the node's edit page'
      //drupal_goto('node/' . $nid . '/edit', drupal_get_destination());
      exit(t('MobWrite access is set for only the initiating editor to have access.'));

    // share mode is 'url access'
    case 'url':
      // check if url access already set
      if (mobwrite_variable_get('mode_node_' . $nid, '') == 'url') {
        //drupal_set_message(t('Welcome to collaborative node editing with MobWrite by URL Access.'), 'status', FALSE);
        exit(t('Welcome to collaborative node editing with MobWrite by URL Access.'));
      }
      else {
        //drupal_set_message(l(t('Share this URL '), 'node/' . $nid . '/edit/mobwrite/share/url/' . $key) . t('with users you want to share editing with.'), 'status', FALSE);
        // set mode to 'url access'
        mobwrite_variable_set('mode_node_' . $nid, 'url');
        exit(l(t('Share this URL '), 'node/' . $nid . '/edit/' . $key) . t('with users you want to share editing with.'));

      }
      // return to the the node's edit page'
      //drupal_goto('node/' . $nid . '/edit', drupal_get_destination());

    // share mode is 'role access'
    case 'role':
      // check if role access already set
      if (mobwrite_variable_get('mode_node_' . $nid, '') == 'role')
        //drupal_set_message(t('Welcome to collaborative node editing with MobWrite by User Role Access.'), 'status', FALSE);
        exit(t('Welcome to collaborative node editing with MobWrite by User Role Access.'));

      else {
        //drupal_set_message(t('Collaborative node editing is now being shared with all users that have proper node permissions.'), 'status', FALSE);
        // set mode to 'role access'
        mobwrite_variable_set('mode_node_' . $nid, 'role');
        exit(t('Collaborative node editing is now being shared with all users that have proper node permissions.'));
      }
      // return to the the node's edit page'
      //drupal_goto('node/' . $nid . '/edit', drupal_get_destination());

  }// switch ($mode)
} // mobwrite_share_mode()


/**
 * @todo Evaluate utilizing user_access('access mobwrite')
 *
 * Verifies and grants users 'update' access with a share mode bias.
 * Employs node_access()
 *
 * @param $node
 *  The Drupal node of the share mode to set.
 * @param $key
 *  Used verify the callback URL by comparing param to the share group from db.
 *
 * @return
 *  True or False on wither the Global user has 'update' access.
 */
function mobwrite_node_update_access($node = NULL, $key = NULL) {
  // get the current node's share mode
  $mode = mobwrite_variable_get('mode_node_' . $node->nid, '');
  $key = ($key == NULL) ? arg(3) : $key;

  // different actions for each share mode
  switch ($mode) {
    // share mode 'no access' - yes the primary user still has access.
    case 'no':
      if (!mobwrite_participant_is(NULL, $node->nid)) {
        return node_access('update', $node);
      }
      elseif (mobwrite_participant_is($GLOBALS['user']->uid, $node->nid)) {
        return node_access('update', $node);
      }

    // share mode 'url access'
    case 'url':
      $share_group = mobwrite_variable_get('share_group_node_' . $node->nid, NULL);
      // verify $key is authentic, if T, grant access

      if ($key != NULL && $key == $share_group ) {
        //drupal_add_js('jQuery(document).ready(function () { mobwriteShare(Drupal.settings.mobwrite.formID); });', 'inline');
        return node_access('update', $node);
      }
      // even with out $key, if the user is an existing participant, grant access
      elseif (mobwrite_participant_is($GLOBALS['user']->uid, $node->nid)) {
        //drupal_add_js('jQuery(document).ready(function () { mobwriteShare(Drupal.settings.mobwrite.formID); });', 'inline');
        return node_access('update', $node);
      }

    // share mode 'role access' - grants access to users viewing node/%node/edit
    case 'role':
      // @todo need to check for key
      //if (user_access('access mobwrite'))
      //drupal_add_js('jQuery(document).ready(function () { mobwriteShare(Drupal.settings.mobwrite.formID); });', 'inline');
      return node_access('update', $node);
      break;

    // case of the share mode not yet being set, such as on initial load. Set to 'no access'.
    case '':
      mobwrite_variable_set('mode_node_' . $node->nid, 'no');
      return node_access('update', $node);

    default:
      return FALSE;

  } // switch ($mode)
} // mobwrite_node_update_access()


/**
 * @todo expand user message to be more helpful.
 *
 * Allows a "timeout" callback to return the webclient from node/%node/edit to
 * node/%node should the client be abandoned by the user. This will allow the
 * user''s participation in the session to expire.
 * Also calls drupal_set_message().
 *
 * @param $node
 *  The Drupal node of the share mode to set.
 */
function mobwrite_return($node) {
  drupal_set_message('Your MobWrite Session has timed out. You can return by clicking the edit tab or, if applicable, via the tokenized URL.', 'status', FALSE);
  // Clear the MobWrite Session data (one user at a time...)
  mobwrite_session_clear($node->nid);
  // Take the user back to the node view
  drupal_goto('node/' . $node->nid, drupal_get_destination());
} // mobwrite_return()


/**
 * Allows Key'd' URL Access to a MobWrite Session. Verifies passed $key and
 * Node's share mode.
 *
 * Also calls drupal_set_message().
 *
 * @param $node
 *  The Drupal node of the share mode to set.
 */
function mobwrite_url($node=NULL, $key=NULL) {
  drupal_set_message('Your MobWrite Session has timed out. You can return by clicking the edit tab or, if applicable, via the tokenized URL.', 'status', FALSE);
  // Remove user from participants
  mobwrite_participant_del($GLOBALS['user']->uid);
  // Take the user back to the node view
  drupal_goto('node/' . $node->nid, drupal_get_destination());
} // mobwrite_url()


/**
 * @todo The UI leaves much to be desired.
 * @link Tracking here. Link http://drupal.org/node/883574 @endlink
 *
 * Builds the module's User Interface for mobwrite_form_alter()
 *
 * @return
 *  Returns a $form array to be merged with $form in mobwrite_form_alter()
 */
function mobwrite_add_ui() {
  // use type fieldset to gather all the elements together
  $form['mobwrite_ui'] = array(
    '#type' => 'fieldset',
    '#title' => t('MobWrite'),
  );

  // add links for share options
  $form['mobwrite_ui']['share_options'] = array(
    '#type' => 'markup',
    '#markup' =>
      '<a href="#" class="mobwrite-share-role">Share by User Role</a>  |
       <a href="#" class="mobwrite-share-url">Share by URL</a>  |
       <a href="#" class="mobwrite-share-stop">Stop Sharing</a><br>',
  );

  // provides an HTML area to set module's status messages
  $form['mobwrite_ui']['message'] = array(
    '#type' => 'markup',
    '#markup' => '<strong><div id="mobwrite-message"></div></strong><br>',
  );

  // provides an HTML area to display which usernames are participating in a given session
  $form['mobwrite_ui']['participants']['#markup'] = '<em>MobWrite Participants:</em>
    <div id="mobwrite-participants"></div>';

  return $form;
} // mobwrite_add_ui()


/**
 * @todo Better handleing of invaild $key values. Intended method with
 * drupal_goto() does not work and is commented out below. This is in reference
 * to @link MobWrite Issue. Link http://drupal.org/node/883478 @endlink
 *
 * Tracks which users are in a MobWrite session for which node. Allows callback
 * to set user's timestamp and automatically removes users that have not
 * "pinged" for 15 seconds.
 *
 * @param $node
 *  The Drupal node of the share mode to set.
 * @param $key
 *  Used verify the callback URL by comparing param to the share group from db.
 * @param $uid
 *  The user to set in the the share group.
 *
 * @return
 *  Does not return data, but exit()s with formated HTML list of usernames in a
 *  share group.
 */
function mobwrite_participant($node = NULL, $key = NULL, $uid = NULL) {
  $nid = $node->nid;
  $share_group = mobwrite_variable_get('share_group_node_' . $nid, 'not set');
  // authenticate $key
  if ($key != $share_group && $share_group != 'not set') {
    // @todo drupal_goto messes stuff up in a way I do not understand :)
    //  need a better work around. http://drupal.org/node/883478
    //drupal_set_message(t('The key provided does not match the MobWrite Session:') . '<br>' . $key, 'status', TRUE);
    //drupal_goto('node/' . $nid, drupal_get_destination());
    exit(t('No participants were found because the key provided does not match the MobWrite Session.'));
  }

  // merge the passed UID
  db_merge('mobwrite_participants')
    ->key(array('uid' => $uid, 'nid' => $nid))
    ->fields(array('timestamp' => REQUEST_TIME))
    ->execute();

  // remove expired participants
  db_delete('mobwrite_participants')
   ->condition('timestamp', REQUEST_TIME - 15, '<')
   ->execute();

  // gather up participants for return
  $query = db_select('mobwrite_participants', 'mp');
  // join with users table to get the participant's username
  $query->join('users', 'u', 'mp.uid = u.uid');
  // add participant  fields
  $query->addField('mp', 'uid');
  $query->addField('mp', 'nid');
  // add username fields
  $query->addField('u', 'name');
  // set the condition to be uids assoc with nid
  $query->condition('mp.nid', $nid);
  // order by who has been there the longest
  $query->orderBy('mp.timestamp', 'DESC');
  // catch the results of the query
  $results = $query->execute();

  // start our formated output
  $output = '<div id="mobwrite-participants">';
  $output .= '<ul>';
  //now find our users
  foreach ($results as $record) {
    $output .= '<li>' . theme('username', array('account' => $record)) . '</li>';
  }
  // close the markup
  $output .= '</ul></div>';

  exit($output);
} // mobwrite_participant()


/**
 * Check wither a user is in a share group or if the share group has users,
 * depending on the passed parameters.
 *
 * Users are timestamped and automatically removed when users that have not
 * "pinged" for 15 seconds.
 *
 * @param $uid
 *  The user to set in the the share group.
 * @param $nid
 *  The ID of the Drupal node to check against.
 *
 * @return
 *  FALSE if the UID is not in the NIDs share group, otherwise the UID.
 *  FALSE if the passed $uid = NULL and there are no UIDs set for the passed
 *  $nid.
 */
function mobwrite_participant_is($uid = NULL, $nid = NULL) {

  if ($uid == NULL) {
    // return T or F for if there are participants for $nid
    $query = db_select('mobwrite_participants', 'mp');
    // select fields
    $query->fields('mp', array('uid'));
    // set condition
    $query->condition('mp.nid', $nid);
    // catch the results of the query
    $results = $query->execute();

    return ($results) ? $results->fetchField() : FALSE;
  }

  // first check if the UID is valid.
  $account = user_load($uid);
  if (!$account === FALSE) {
    $query = db_select('mobwrite_participants', 'mp');

    $results = $query
      ->fields('mp', array('uid'))
      ->condition('mp.uid', $account->uid)
      ->condition('mp.nid', $nid)
      ->execute();

    return ($results) ? $results->fetchField() : FALSE;
  }
  return FALSE;
} //mobwrite_participant_is()


/**
 * @todo test of failure
 *
 * Removes passed(UID) user object from participant table. If passed $uid is
 * NULL, and $nid is TRUE, deletes users associated with the $nid - Used when
 * a MobWrite session terminates or the share mode changes.
 *
 * @param $uid
 *  The user to remove.
 * @param $nid
 *  The ID of the Drupal node to remove the passed $uid from, or to empty.
 *
 * @return
 *  Returns false if UID is not a participant or if delete fails.
 */
function mobwrite_participant_del($uid = NULL, $nid = NULL) {
  $account = user_load($uid);
  if ($account != FALSE) {
    db_delete('mobwrite_participants')
      ->condition('uid', $account->uid)
      ->condition('nid', $nid)
      ->execute();
  }
  if ($account == FALSE && $nid > 0) {
    db_delete('mobwrite_participants')
      ->condition('nid', $nid)
      ->execute();
  }
} //mobwrite_participant_del()


/**
 * @todo Provide more helpful info to the user - link to help?
 *
 * Sets Drupal status messages to the user concerning MobWrite
 *
 * @param $user
 *  The user to receive the message.
 */
function mobwrite_set_status_message($user) {
      $nid = menu_get_object()->nid;
      $share_group = mobwrite_variable_get('share_group_node_' . $nid, ' * mobwrite_share_group is not set * ');

      // @todo more info for false message about how edits are handled.
      // message for with out mobwrite session access. Check is participants exist, then if the user is one.
      if (mobwrite_participant_is(NULL, $nid) && !mobwrite_participant_is($user->uid, $nid)) {
        drupal_set_message(t('Sorry, a MobWrite editing session, that you do not have access to, is taking place.'), 'status', TRUE);
      }

      // @todo What about a dump of all mobwrite vars?
      // simple debug message
      if (variable_get('mobwrite_debug', TRUE)) {
        drupal_set_message('Debug info: ' . $share_group . '@' . variable_get('mobwrite_sync_gateway', ' * mobwrite_sync_gateway is not set * '), 'status', TRUE);
      }
} // mobwrite_set_status_message()


/**
 * Builds an array of MobWrite Session settings for a given $form.
 *
 * @param $form
 *  The form to be edit with MobWrite and to build settings array for.
 *
 * @return
 *  Returns array of key'd values to be attached to a form in
 *  mobwrite_form_alter()
 */
function mobwrite_set_settings($form) {
  $nid = menu_get_object()->nid;
  // Create share group for this instance. Need the timestamp the URL access key is unique to time as well as resource
  if (!mobwrite_variable_get('share_group_node_' . $nid, FALSE)) {
    mobwrite_share_group_reset();
  }

  return array(
    'nid' => $nid,
    'uid' => $GLOBALS['user']->uid,
    'formID' => $form['#id'],
    'elementID' => 'edit-body-und-0-value',
    'shareGroup' => mobwrite_variable_get('share_group_node_' . $nid, ' * mobwrite_share_group  is not set * '),
    'syncGateway' => variable_get('mobwrite_sync_gateway', '* mobwrite_sync_gateway  is not set * '),
    'debugOn' => (variable_get('mobwrite_debug', TRUE)) ? true : false,
    'timeout' => variable_get('mobwrite_timeout', 300),
  );
} // mobwrite_set_setttings()


/**
 * Resets the share group. Also used to create the share group.
 */
function mobwrite_share_group_reset() {
  $nid = menu_get_object()->nid;
  // delete existing share_group
  mobwrite_variable_del('share_group_node_' . $nid);
  // create new share_group
  $share_group = 'mobwrite-' . drupal_hash_base64(variable_get('mobwrite_site_id', '') . str_replace('/', '', request_path()) . REQUEST_TIME);
  mobwrite_variable_set('share_group_node_' . $nid, $share_group);
  // update the attached shareGroup value so JavaScript callbacks have the
  // new share_group
  //drupal_add_js(array('mobwrite' => array('shareGroup' => $share_group)), 'setting');

} // mobwrite_share_group_reset()


/**
 * @todo cache the data?
 *
 * Retrieves "MobWrite session-like" data from key'd value table.
 *
 * @param $name
 *  The name of the data to get.
 * @param $default
 *  The data to return should the query fail.
 *
 * @return
 *  The corresponding data payload or $default if the query fails.
 */
function mobwrite_variable_get($name, $default = NULL) {
  $query = db_select('mobwrite_variable', 'mv');
  $query ->condition('mv.name', $name, '=') ->fields('mv', array('value'));
  $result = unserialize($query->execute()->fetchField());
  return ($result) ? $result : $default;
} // mobwrite_variable_get()


/**
 * @todo cache the data?
 *
 * Stores "MobWrite session-like" data in key'd value table.
 *
 * @param $name
 *  The name of the data to store.
 * @param $value
 *  The data to store.
 */
function mobwrite_variable_set($name, $value) {
  db_merge('mobwrite_variable') ->key(array('name' => $name)) ->fields(array('value' => serialize($value),)) ->execute();
} // mobwrite_variable_set()


/**
 * @todo cache the data?
 *
 * Deletes "MobWrite session-like" data from key'd value table.
 *
 * @param $name
 *  The name of the data delete.
 */
function mobwrite_variable_del($name) {
  db_delete('mobwrite_variable') ->condition('name', $name) ->execute();
} // mobwrite_variable_del()
