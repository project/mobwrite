
/**
 * @file
 * Supportive MobWrite Module JavaScript functions.
 *
 */

(function($) {
  // variables for the timeout feature
  var time = null;
  var timerID = null;
  var timeout = null;
  var timeoutIdle = null;
  var timeoutAway = null;
  var participantIntervalID = null;


  /**
  * @todo expand UI to be more helpfull
  * @see http://drupal.org/node/883574
  *
  * @todo if share mode is active, then mobwriteActivate() should get shareing
  *   going with out requirng user action.
  * @see http://drupal.org/node/886054
  *
  * mobwriteActivate() sets up a mobwrite session by setting the syncGateway,
  * idPrefix (shareGroup), debug option, and setup up the timer.
  *
  * Next is actions for the UI controls.
  */
  Drupal.behaviors.mobwriteActivate = {
    attach: function (context) {
      mobwrite.syncGateway = Drupal.settings.mobwrite.syncGateway;
      mobwrite.idPrefix = Drupal.settings.mobwrite.shareGroup;
      mobwrite.debug = Drupal.settings.mobwrite.debugOn;

      // set up for timer
      mobwriteTimeoutInit();

      // check if share mode is all ready set
      mobwriteShareMode('get');

      // ui to share by role
      $('a.mobwrite-share-role').click(function() {
        // set share mode
        mobwriteShareMode('role');
      });

      $('a.mobwrite-share-url').click(function() {
        // set share mode
        mobwriteShareMode('url');
      });

      $('a.mobwrite-share-stop').click(function() {
        //stop the setInterval below that updates the participant html area
        clearInterval(participantIntervalID);
        // "unshare" the form. mobwrite.unshare does not accept forms.
        mobwrite.shared = {};
        // set share mode
        mobwriteShareMode('no');
      });
    }
  }; //mobwriteActivate()


  /**
   * Wrapper share function to keep the three action items together
   */
  function mobwriteShare() {
    // share the form or edit-body element
    //mobwrite.share(Drupal.settings.mobwrite.formID);
    mobwrite.share(Drupal.settings.mobwrite.elementID);
    // track and display participants
    mobwriteParticipant();
    // run timer
    mobwriteTimer();
  } //mobwriteShare()


  /**
   * Sets the share mode via callback and sets MobWrite Status Message with
   * callback's response.
   *
   * @param mode
   *  The mode to set. Options are 'url', 'no', and 'role'. By passing 'get'
   *  the callback will be queried for an existing share mode.
   */
  function mobwriteShareMode(mode) {
    // set up the path
    var path = Drupal.settings.basePath + 'node/' + Drupal.settings.mobwrite.nid + '/edit/mobwrite/share/';

    // Run the switch for options based on mode
    switch (mode) {
      // mode 'get' pings callback to get existing set mode.
      case 'get':
        $.get(path + 'get/' + Drupal.settings.mobwrite.shareGroup, function(data) {
          //strip HTML tags and set mode value
          mode = data.replace(/(<([^>]+)>)/gi, '');
        });
        // exit mobwriteShareMode if 'get' results in mode not set.
        if (mode == 'not set') return;
        // break; - no break so the remaining cases are matched.

      // share mode is 'no access'
      case 'no':
        path += 'no/' + Drupal.settings.mobwrite.shareGroup;
        break;

      // share mode is 'url access'
      case 'url':
        path += 'url/' + Drupal.settings.mobwrite.shareGroup;
        alert ("MobWrite Status message does not update, bug #886054, use this Drupal path to try out URL Access: " +
          'node/' + Drupal.settings.mobwrite.nid  + '/edit/' + Drupal.settings.mobwrite.shareGroup);
        break;

      // share mode is 'role access'
      case 'role':
        path += 'role/' + Drupal.settings.mobwrite.shareGroup;
        break;

    } // switch (mode)

    // now we have the correct path and can make the call to update the
    // mobwrite status message and set the mode
    $.get(path, function(data) {
      $('#mobwrite-message').replaceWith(data);
    });

    // check if mobwriteShare() should be called.
    // couldnt do it in switch because message wouldnt get set?
    // maybe related to http://drupal.org/node/886054
    if (mode == 'url' || mode == 'role') {
      mobwriteShare();
    }
  } // mobwriteShareMode()

  /**
   * Accesses callback to sets users as share group participants and updates
   * participant list with returned data.
   *
   */
  function mobwriteParticipant() {
    participantIntervalID = setInterval(function() {
      var path = Drupal.settings.basePath + 'node/' + Drupal.settings.mobwrite.nid + '/edit/mobwrite/participant/' +
        Drupal.settings.mobwrite.shareGroup + '/' + Drupal.settings.mobwrite.uid;
      //alert(path);
      $.get(path, function(data) {
          $('#mobwrite-participants').replaceWith(data);
      });
    }, 1500);
  } // mobwriteParticipant()

  // Following provides timeout logic for the module''s autosave/timeout
  // They are not follow commented and scrubbed. I will be looking to simpler
  // implementation using setInterval (didnt know it existed before)
  // Also need to reevaluate the use case of the timeout since the autosave was
  // decided against and with the participant update feature above.

  // Sets up the timer and calls the timer
  function mobwriteTimeoutInit() {
    timeout = Drupal.settings.mobwrite.timeout * 1000;
    $(document).mousemove(function(event) { mobwriteTimeoutReset(event); });
    mobwriteTimeoutReset();
  }

  // Timer checks for Idle and Away activity thresholds if not breached,
  // reset timeouts and call timer again.
  function mobwriteTimer() {
    time = new Date().getTime();

    if (timeoutAway < time) {
      mobwriteTimeoutAway();
    }
    else {
      timerID = setTimeout(mobwriteTimer, timeout);
    }

    if (timeoutIdle < time) {
      mobwriteTimeoutIdle();
    }
    else {
      timerID = setTimeout(mobwriteTimer, timeout);
    }
  }

  // user as idle, display warning of of time out
  function mobwriteTimeoutIdle() {
    //alert('Your MobWrite Participation is about to timeout.');
  }

  // user is away, end editing session
  function mobwriteTimeoutAway() {
      window.location = Drupal.settings.basePath + 'node/' + Drupal.settings.mobwrite.nid + '/edit/mobwrite/return';
  }

  // reset the timers
  function mobwriteTimeoutReset() {
      timeoutAway = new Date().getTime() + timeout;
      timeoutIdle = timeoutAway * 0.75;
  }

})(jQuery);