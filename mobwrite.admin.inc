<?php

/**
 * @file
 * MobWrite module settings UI.
 */

/**
 * Menu callback; displays the MobWrite administration page.
 */
function mobwrite_admin_overview() {
  // Settings for Google's Mobwrite
  $form['mobwrite_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mobwrite Settings'),
  );

  $form['mobwrite_settings']['mobwrite_sync_gateway'] = array(
    '#type' => 'textfield',
    '#title' => t('Sync Gateway:'),
    '#default_value' => variable_get('mobwrite_sync_gateway', 'http://mobwrite3.appspot.com/scripts/q.py'),
    '#description' => t('The URL or local path of the sync gateway to use.'),
    '#required' => TRUE,
  );

  $form['mobwrite_settings']['mobwrite_library_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Library Path:'),
    '#default_value' => variable_get('mobwrite_library_path', 'sites/all/libraries/google_mobwrite/html'),
    '#description' => t('The local path of the Google MobWrite library directory.'),
    '#required' => TRUE,
  );

  $form['mobwrite_settings']['mobwrite_timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('Timeout:'),
    '#size' => 6,
    '#field_suffix' => t('seconds'),
    '#default_value' => variable_get('mobwrite_timeout', 300),
    '#description' => t('Time wait on a collaborator before assuming they have left the session.'),
    '#required' => TRUE,
  );

  $form['mobwrite_settings']['misc'] = array(
    '#markup' => '<strong>' . t('Miscellaneous:') . '</strong>',
  );

  $form['mobwrite_settings']['mobwrite_debug'] = array(
    '#title' => t('MobWrite Debug'),
    '#type' => 'checkbox',
    '#return_value' => 1,
    '#default_value' => variable_get('mobwrite_debug', TRUE),
    '#description' => t('Assist troubleshooting by loading non-compressed libraries and setting mobwrite.debug = TRUE.'),
  );

  // Show checkboxes for every content type, allowing the user to enable/disable
  // MobWrite for that content type.
  $form['mobwrite_settings']['mobwrite_enable'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enable MobWrite on content types'),
    '#default_value' => variable_get('mobwrite_enable', array()),
    '#options' => node_type_get_names(),
    '#description' => t('MobWrite will be enabled for users with the correct permissions on these content types.'),
  );

  return system_settings_form($form);
} // mobwrite_admin_overview()